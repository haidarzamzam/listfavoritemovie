package com.haidev.listmoviefavorite.main.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class FavMovieModel(
    val id: Long,
    val title: String,
    val overview: String,
    val posterPath: String,
    val releaseDate: String

) : Parcelable {

    // variable yang akan menjadi tag
    companion object {
        const val TABLE_FAVORITE_MOVIE: String = "TABLE_FAVORITE_MOVIE"
        const val ID_FAVORITE_MOVIE: String = "ID_FAVORITE_MOVIE"
        const val TITLE_FAVORITE_MOVIE: String = "TITLE_FAVORITE_MOVIE"
        const val OVERVIEW_FAVORITE_MOVIE: String = "OVERVIEW_FAVORITE_MOVIE"
        const val POSTER_FAVORITE_MOVIE: String = "POSTER_FAVORITE_MOVIE"
        const val DATE_FAVORITE_MOVIE: String = "DATE_FAVORITE_MOVIE"

    }


}
