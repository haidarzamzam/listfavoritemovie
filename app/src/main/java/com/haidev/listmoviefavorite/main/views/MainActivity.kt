package com.haidev.listmoviefavorite.main.views

import android.content.Context
import android.database.ContentObserver
import android.database.Cursor
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.listmoviefavorite.R
import com.haidev.listmoviefavorite.databinding.ActivityMainBinding
import com.haidev.listmoviefavorite.db.MyDatabaseOpenHelper.Companion.CONTENT_URI_MOVIE
import com.haidev.listmoviefavorite.main.adapters.ItemMovieAdapter
import com.haidev.listmoviefavorite.main.models.FavMovieModel.Companion.DATE_FAVORITE_MOVIE
import com.haidev.listmoviefavorite.main.models.FavMovieModel.Companion.ID_FAVORITE_MOVIE
import com.haidev.listmoviefavorite.main.models.FavMovieModel.Companion.OVERVIEW_FAVORITE_MOVIE
import com.haidev.listmoviefavorite.main.models.FavMovieModel.Companion.POSTER_FAVORITE_MOVIE
import com.haidev.listmoviefavorite.main.models.FavMovieModel.Companion.TITLE_FAVORITE_MOVIE
import com.haidev.listmoviefavorite.main.models.MovieModel
import com.haidev.listmoviefavorite.main.viewmodels.MainViewModel
import java.lang.ref.WeakReference


class MainActivity : AppCompatActivity(), LoadMoviesCallback {

    private lateinit var dataObserver: DataObserver
    private lateinit var mainBinding: ActivityMainBinding
    private lateinit var vmMain: MainViewModel
    private lateinit var adapter: ItemMovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        vmMain = ViewModelProviders.of(this).get(MainViewModel::class.java)
        mainBinding.main = vmMain

        Log.i("CONSUMERS", CONTENT_URI_MOVIE.toString())

        val handlerThread = HandlerThread("DataObserver")
        handlerThread.start()
        val handler = Handler(handlerThread.looper)
        dataObserver = DataObserver(handler, this)
        contentResolver.registerContentObserver(CONTENT_URI_MOVIE, true, dataObserver)
        GetData(this, this).execute()
        //
    }


    override fun postExecute(data: Cursor) {
        val datas = mapCursorToArrayList(data)

        if (datas.isNotEmpty()) {
            for (i in 0 until datas.size) {
                Log.i("CONSUMERS", datas[i].title)
                val lManager = LinearLayoutManager(this)
                mainBinding.rvListMovie.layoutManager = lManager
                mainBinding.rvListMovie.setHasFixedSize(true)

                adapter = ItemMovieAdapter(this, datas)
                mainBinding.rvListMovie.adapter = adapter
            }
        } else {
            Log.i("CONSUMERS", "EMPTY")
        }
    }


    internal class DataObserver(handler: Handler, private val context: Context) : ContentObserver(handler) {
        override fun onChange(selfChange: Boolean) {
            super.onChange(selfChange)
            GetData(context, context as MainActivity).execute()
        }
    }

    private class GetData(context: Context, callback: LoadMoviesCallback) : AsyncTask<Void, Void, Cursor>() {
        private val weakContext = WeakReference(context)
        private val weakCallback = WeakReference(callback)


        override fun doInBackground(vararg voids: Void): Cursor? {
            return weakContext.get()?.contentResolver?.query(CONTENT_URI_MOVIE, null, null, null, null)
        }

        override fun onPostExecute(data: Cursor) {
            super.onPostExecute(data)
            weakCallback.get()?.postExecute(data)
        }

    }

}

interface LoadMoviesCallback {
    fun postExecute(data: Cursor)
}

fun mapCursorToArrayList(notesCursor: Cursor): List<MovieModel> {
    val data = mutableListOf<MovieModel>()

    while (notesCursor.moveToNext()) {
        val id = notesCursor.getString(notesCursor.getColumnIndexOrThrow(ID_FAVORITE_MOVIE))
        val title = notesCursor.getString(notesCursor.getColumnIndexOrThrow(TITLE_FAVORITE_MOVIE))
        val posterPath = notesCursor.getString(notesCursor.getColumnIndexOrThrow(POSTER_FAVORITE_MOVIE))
        val description = notesCursor.getString(notesCursor.getColumnIndexOrThrow(OVERVIEW_FAVORITE_MOVIE))
        val date = notesCursor.getString(notesCursor.getColumnIndexOrThrow(DATE_FAVORITE_MOVIE))
        data.add(MovieModel(id, title, posterPath, description, date))
    }

    return data
}

