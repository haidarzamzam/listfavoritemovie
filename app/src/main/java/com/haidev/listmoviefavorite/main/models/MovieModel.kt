package com.haidev.listmoviefavorite.main.models

data class MovieModel(
    val id: String,
    val title: String,
    val posterPath: String,
    val overview: String,
    val releaseDate: String
)