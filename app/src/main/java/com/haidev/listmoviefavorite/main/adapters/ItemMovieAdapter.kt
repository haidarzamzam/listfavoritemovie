package com.haidev.listmoviefavorite.main.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.listmoviefavorite.R
import com.haidev.listmoviefavorite.databinding.ItemListMovieBinding
import com.haidev.listmoviefavorite.main.models.MovieModel
import com.haidev.listmoviefavorite.main.viewmodels.ItemListMovieViewModel

class ItemMovieAdapter(private val context: Context, private var listFavMovie: List<MovieModel>) :
    RecyclerView.Adapter<ItemMovieAdapter.ItemMovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemMovieViewHolder {
        val binding: ItemListMovieBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_list_movie, parent, false)
        return ItemMovieViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listFavMovie.size
    }

    override fun onBindViewHolder(holder: ItemMovieViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(listFavMovie[fixPosition])
    }

    class ItemMovieViewHolder(val binding: ItemListMovieBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemListMovieViewModel

        fun bindBinding(model: MovieModel) {
            viewModel = ItemListMovieViewModel(model, binding)
            binding.itemMovie = viewModel
            binding.executePendingBindings()
        }

    }
}