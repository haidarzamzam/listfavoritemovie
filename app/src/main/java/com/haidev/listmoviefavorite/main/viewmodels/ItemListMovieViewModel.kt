package com.haidev.listmoviefavorite.main.viewmodels

import androidx.databinding.ObservableField
import com.haidev.listmoviefavorite.databinding.ItemListMovieBinding
import com.haidev.listmoviefavorite.main.models.MovieModel
import com.squareup.picasso.Picasso

class ItemListMovieViewModel(
    val model: MovieModel,
    binding: ItemListMovieBinding
) {

    var title: ObservableField<String?> = ObservableField(model.title)
    var desc: ObservableField<String?> = ObservableField(model.overview)
    var date: ObservableField<String?> = ObservableField(model.releaseDate)

    init {

        Picasso.get().load("https://image.tmdb.org/t/p/w185" + model.posterPath).into(binding.ivImageMovie)
    }


}