package com.haidev.listmoviefavorite.main.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class MainViewModel(application: Application) : AndroidViewModel(application)